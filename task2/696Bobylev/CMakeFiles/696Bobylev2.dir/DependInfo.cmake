# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/gas/git/miptgraph/opengl_tasks_2020/task2/696Bobylev/Application.cpp" "/home/gas/git/miptgraph/opengl_tasks_2020/task2/696Bobylev/CMakeFiles/696Bobylev2.dir/Application.cpp.o"
  "/home/gas/git/miptgraph/opengl_tasks_2020/task2/696Bobylev/Camera.cpp" "/home/gas/git/miptgraph/opengl_tasks_2020/task2/696Bobylev/CMakeFiles/696Bobylev2.dir/Camera.cpp.o"
  "/home/gas/git/miptgraph/opengl_tasks_2020/task2/696Bobylev/Mesh.cpp" "/home/gas/git/miptgraph/opengl_tasks_2020/task2/696Bobylev/CMakeFiles/696Bobylev2.dir/Mesh.cpp.o"
  "/home/gas/git/miptgraph/opengl_tasks_2020/task2/696Bobylev/ShaderProgram.cpp" "/home/gas/git/miptgraph/opengl_tasks_2020/task2/696Bobylev/CMakeFiles/696Bobylev2.dir/ShaderProgram.cpp.o"
  "/home/gas/git/miptgraph/opengl_tasks_2020/task2/696Bobylev/Texture.cpp" "/home/gas/git/miptgraph/opengl_tasks_2020/task2/696Bobylev/CMakeFiles/696Bobylev2.dir/Texture.cpp.o"
  "/home/gas/git/miptgraph/opengl_tasks_2020/task2/696Bobylev/main.cpp" "/home/gas/git/miptgraph/opengl_tasks_2020/task2/696Bobylev/CMakeFiles/696Bobylev2.dir/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GLEW_STATIC"
  "GLFW_DLL"
  "GLM_FORCE_DEPTH_ZERO_TO_ONE"
  "GLM_FORCE_PURE"
  "GLM_FORCE_RADIANS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "external/glew-1.13.0/include"
  "external/GLM"
  "external/SOIL/src/SOIL2"
  "external/Assimp/include"
  "external/GLFW/include"
  "external/imgui"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/gas/git/miptgraph/opengl_tasks_2020/external/glew-1.13.0/build/cmake/CMakeFiles/glew_s.dir/DependInfo.cmake"
  "/home/gas/git/miptgraph/opengl_tasks_2020/external/SOIL/CMakeFiles/soil.dir/DependInfo.cmake"
  "/home/gas/git/miptgraph/opengl_tasks_2020/external/Assimp/code/CMakeFiles/assimp.dir/DependInfo.cmake"
  "/home/gas/git/miptgraph/opengl_tasks_2020/external/imgui/CMakeFiles/imgui.dir/DependInfo.cmake"
  "/home/gas/git/miptgraph/opengl_tasks_2020/external/Assimp/contrib/irrXML/CMakeFiles/IrrXML.dir/DependInfo.cmake"
  "/home/gas/git/miptgraph/opengl_tasks_2020/external/GLFW/src/CMakeFiles/glfw.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
