#include <iostream>
#include <vector>
#include <time.h>

#include "Application.hpp"
#include "LightInfo.hpp"
#include "Mesh.hpp"
#include "ShaderProgram.hpp"
#include "Texture.hpp"


class SampleApplication : public Application
{
public:
    MeshPtr _cube;

    std::vector<MeshPtr>* _tree;
    std::vector<MeshPtr> _leafs;

    ShaderProgramPtr _shader;

    float _lr = 3.0;
    float _phi = 0.0;
    float _theta = glm::pi<float>() * 0.25f;

    LightInfo _light;

    TexturePtr _worldTexture;

    TexturePtr _leafTexture;

    GLuint _sampler;

    GLuint _samplerLeaf;

    void makeScene() override
    {
        srand(time(NULL));
        Application::makeScene();

        _tree = makeTree(_leafs);

        //=========================================================
        //Инициализация шейдеров

        _shader = std::make_shared<ShaderProgram>("696BobylevData2/texture.vert", "696BobylevData2/texture.frag");

        //=========================================================
        //Инициализация значений переменных освщения
        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        _light.ambient = glm::vec3(0.2, 0.2, 0.2);
        _light.diffuse = glm::vec3(0.8, 0.8, 0.8);
        _light.specular = glm::vec3(1.0, 1.0, 1.0);

        //=========================================================
        //Загрузка и создание текстур
        _worldTexture = loadTexture("696BobylevData2/tex.jpg");

        _leafTexture = loadTexture("696BobylevData2/leaf.png");

        //=========================================================
        //Инициализация сэмплера, объекта, который хранит параметры чтения из текстуры
        glGenSamplers(1, &_sampler);
        glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);	
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    }

    void update() override
    {
        Application::update();
    }

    void draw() override
    {
        Application::draw();

        //Получаем текущие размеры экрана и выставлям вьюпорт
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Подключаем шейдер
        _shader->use();

         //Устанавливаем общие юниформ-переменные
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 1.0));

        _shader->setVec3Uniform("light.pos", lightPosCamSpace); //копируем положение уже в системе виртуальной камеры
        _shader->setVec3Uniform("light.La", _light.ambient);
        _shader->setVec3Uniform("light.Ld", _light.diffuse);
        _shader->setVec3Uniform("light.Ls", _light.specular);

        GLuint textureUnitForDiffuseTex = 0;
       
        glBindTextureUnit(textureUnitForDiffuseTex, _worldTexture->texture());
        glBindSampler(textureUnitForDiffuseTex, _sampler);
        _shader->setIntUniform("diffuseTex", textureUnitForDiffuseTex);

        for (MeshPtr meshPtr : *_tree)
        {
            _shader->setMat4Uniform("modelMatrix", meshPtr->modelMatrix());
            _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(meshPtr->modelMatrix()))));
            meshPtr->draw();
        }

        glBindTextureUnit(textureUnitForDiffuseTex, _leafTexture->texture());
        glBindSampler(textureUnitForDiffuseTex, _sampler);

        for (MeshPtr meshPtr : _leafs)
        {
            _shader->setMat4Uniform("modelMatrix", meshPtr->modelMatrix());
            _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(meshPtr->modelMatrix()))));
            meshPtr->draw();
        }

        //Отсоединяем сэмплер и шейдерную программу
        glBindSampler(0, 0);
        glUseProgram(0);
    }
};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}